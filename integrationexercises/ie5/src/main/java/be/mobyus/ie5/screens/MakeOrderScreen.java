package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;

public class MakeOrderScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(EshopApplicationController controller) {

        System.out.println("Welcome " + controller.getCurrentUser().getFirstName() + ", you are now shopping in " + controller.getChosenShop().getName() + ".");
        System.out.println("Please enter the name of the product (q to quit):");
        return new String[] { readFromConsole("Input") };
    }
}
