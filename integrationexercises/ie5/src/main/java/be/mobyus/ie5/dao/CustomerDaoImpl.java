package be.mobyus.ie5.dao;

import be.mobyus.ie5.entities.Customer;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> findCustomers(String userName) {
        return sessionFactory.getCurrentSession().createQuery("from Customer c where c.userName = :userName")
                .setParameter("userName", userName)
                .list();


    }
}
