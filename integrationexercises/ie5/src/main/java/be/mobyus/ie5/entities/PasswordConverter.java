package be.mobyus.ie5.entities;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Base64;

@Converter
public class PasswordConverter implements AttributeConverter<Password, String> {


    @Override
    public String convertToDatabaseColumn(Password password) {
        String pass = password.getValue();
        String encodedPass = Base64.getEncoder().encodeToString(pass.getBytes());
        return encodedPass;
    }

    @Override
    public Password convertToEntityAttribute(String s) {
        byte[] decodedBytes = Base64.getDecoder().decode(s);
        String decodedString = new String(decodedBytes);
        Password password = new Password(decodedString);
        return password;
    }
}
