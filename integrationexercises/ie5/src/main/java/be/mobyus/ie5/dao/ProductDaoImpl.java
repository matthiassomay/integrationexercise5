package be.mobyus.ie5.dao;

import be.mobyus.ie5.entities.Product;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Product> findProducts(String searchTerm) {
        return sessionFactory.getCurrentSession().createQuery("from Product p where lower(p.name) like :searchTerm")
                .setParameter("searchTerm", ("%" + searchTerm.toLowerCase() + "%"))
                .list();
    }
}
