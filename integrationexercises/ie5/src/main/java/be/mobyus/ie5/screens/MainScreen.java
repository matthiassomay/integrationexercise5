package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;

public class MainScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(EshopApplicationController controller) {

        System.out.println("Welcome " + controller.getCurrentUser().getFirstName() + ", you are now shopping in " + controller.getChosenShop().getName() + ".");
        System.out.println("Make you selection (q to quit):");

        System.out.println("1. Make order");
        System.out.println("2. List orders");

        return new String[] { readFromConsole("Input") };
    }
}
