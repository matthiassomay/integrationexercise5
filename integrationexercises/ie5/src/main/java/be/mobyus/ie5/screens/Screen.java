package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;

public interface Screen {

	public String[] drawScreen(EshopApplicationController controller);

}
