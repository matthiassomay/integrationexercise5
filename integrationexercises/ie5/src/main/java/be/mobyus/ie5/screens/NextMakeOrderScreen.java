package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;
import be.mobyus.ie5.entities.Product;

public class NextMakeOrderScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(EshopApplicationController controller) {

        System.out.println("Welcome " + controller.getCurrentUser().getFirstName() + ", you are now shopping in " + controller.getChosenShop().getName() + ".");
        System.out.println("Searching for products with name “" + controller.getSearchTerm() + "”.");
        System.out.println("To add products to the order now, enter the items comma separated. e. “1,2” will add product");
        System.out.println("one and two to the order. Enter 0 to go back to the previous screen (q to quit).");

        int i = 1;
        for (Product product : controller.getFoundProducts()) {
            System.out.println(i + ". " + product.getName());
            i++;

        }

        return new String[] { readFromConsole("Input") };
    }
}
