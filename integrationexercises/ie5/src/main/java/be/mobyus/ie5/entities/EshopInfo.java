package be.mobyus.ie5.entities;

import javax.persistence.Embeddable;

@Embeddable
public class EshopInfo {

    private String vatnumber;
    private String bankAccountNumber;


    public String getVatnumber() {
        return vatnumber;
    }

    public void setVatnumber(String vatnumber) {
        this.vatnumber = vatnumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }
}
