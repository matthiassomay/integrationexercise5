package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;

public class LoginScreen extends AbstractScreen {

	@Override
	public String[] drawScreenInternal(EshopApplicationController controller) {
		System.out.println("Enter username and password (q to quit).");
		if (controller.isLogin() == false) {
			System.out.println("Error: username or password incorrect");
		}
		return new String[] { readFromConsole("Username"), readFromConsole("Password") };
	}
}
