package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;
import be.mobyus.ie5.entities.Eshop;

public class ShopSelectionScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(EshopApplicationController controller) {


        System.out.println("Welcome " + controller.getCurrentUser().getFirstName() + ", please choose your shop (q to quit):");

        int i = 1;
        for (Eshop shop : controller.getShops()) {
            System.out.println(i + ". " + shop.getName());
            i++;

        }

        return new String[] { readFromConsole("Input") };
    }
}
