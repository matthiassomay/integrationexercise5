package be.mobyus.ie5.dao;

import be.mobyus.ie5.entities.Customer;
import be.mobyus.ie5.entities.Order;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Order> findOrdersForCustomer(Customer customer) {
        return sessionFactory.getCurrentSession().createQuery("select orders from Customer c where c.id = :id")
                .setParameter("id", customer.getId())
                .list();
    }

    @Override
    public void saveOrder(Order order) {
        sessionFactory.getCurrentSession().save(order);
    }
}
