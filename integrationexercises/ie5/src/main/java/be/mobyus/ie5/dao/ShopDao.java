package be.mobyus.ie5.dao;

import java.util.List;

import be.mobyus.ie5.entities.Eshop;

public interface ShopDao {

	List<Eshop> listAllShops();
	Eshop getShop(Long shopId);
}
