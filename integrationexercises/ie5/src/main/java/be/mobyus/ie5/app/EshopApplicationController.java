package be.mobyus.ie5.app;

import be.mobyus.ie5.entities.*;
import be.mobyus.ie5.screens.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import be.mobyus.ie5.service.ShopService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class EshopApplicationController {

	@Autowired
	private ShopService shopService;

	private boolean login = true;
	private boolean shopChosen = false;
	private boolean makeOrder = false;
	private boolean listOrders = false;
	private Customer currentUser = new Customer();
	private Eshop chosenShop = new Eshop();
	private String searchTerm;
	private List<Eshop> shops = new ArrayList<>();
	private List<Product> foundProducts = new ArrayList<>();
	private List<Order> ordersOfCustomer = new ArrayList<>();

	public void run() {
		String[] usernamePassword = getUsernamePassword();
		String user = usernamePassword[0];
		String pass = usernamePassword[1];


		Customer customer;
		try {
			customer = shopService.loadCustomer(user);
			if (customer != null && pass.equals(customer.getPassword().getValue())) {
				currentUser = customer;
				login = true;
			}
		} catch (Exception e) {
			login = false;
			showLoginScreen();
			shopChosen = true;
		}

		while (login) {

			shops = shopService.listEshops();

			String[] selection = showSelectionScreen();

			chosenShop = shops.get(Integer.parseInt(selection[0]) - 1);
			login = false;
			shopChosen = true;


		}

		while (shopChosen) {

			String[] selection = showMainScreen();

			try {
				if (selection[0].equals("1")) {
					makeOrder = true;
					shopChosen = false;
					String[] results = showMakeOrderScreen();
					searchTerm = results[0];
					foundProducts = shopService.listProducts(searchTerm);

					String[] selectedProducts = showNextMakeOrderScreen();
					selectedProducts = selectedProducts[0].split("[,]");

					List<Product> productsChosen = new ArrayList<>();
					for (int i = 0; i < selectedProducts.length ; i++) {
						productsChosen.add(foundProducts.get(Integer.parseInt(selectedProducts[i])));

						Order order = new Order();
						List<OrderDetail> details = new ArrayList<>();
						BigDecimal total = new BigDecimal(0);

						for (Product product : productsChosen) {
							total.add(product.getPrice());
							OrderDetail detail = new OrderDetail();
							detail.setProduct(product);
							detail.setOrder(order);
							detail.setAmount(new BigDecimal(1));
							detail.setTotal(product.getPrice());
							details.add(detail);
						}
						order.setOrderDetails(details);
						order.setEshop(chosenShop);
						order.setPaymentMethod("VISA");
						order.setOrderTotal(total);
						order.setCustomer(currentUser);
						shopService.createOrder(order);

					}


				} else if (selection[0].equals("2")) {
					shopChosen = false;
					ordersOfCustomer = shopService.listOrders(currentUser);
					String[] listOrders = showListOrdersScreen();




				}
			} catch (IndexOutOfBoundsException e) {
				shopChosen = true;
			}
		}


	}

	private String[] getUsernamePassword() {
		return new LoginScreen().drawScreen(this);
	}

	private String[] showMainScreen() {
		return new MainScreen().drawScreen(this);
	}

	private String[] showLoginScreen() { return new LoginScreen().drawScreen(this);}

	private String[] showSelectionScreen() {
		return new ShopSelectionScreen().drawScreen(this);
	}

	private String[] showMakeOrderScreen() {
		return new MakeOrderScreen().drawScreen(this);
	}

	private String[] showNextMakeOrderScreen() {
		return new NextMakeOrderScreen().drawScreen(this);
	}

	private String[] showListOrdersScreen() {
		return new ListOrdersScreen().drawScreen(this);}

	public boolean isLogin() {
		return login;
	}

	public boolean isShopChosen() {
		return shopChosen;
	}

	public boolean isMakeOrder() {
		return makeOrder;
	}

	public boolean isListOrders() {
		return listOrders;
	}

	public Eshop getChosenShop() {
		return chosenShop;
	}


	public String getSearchTerm() {
		return searchTerm;
	}

	public List<Product> getFoundProducts() {
		return foundProducts;
	}

	public List<Eshop> getShops() {
		return shops;
	}

	public Customer getCurrentUser() {
		return currentUser;
	}

	public List<Order> getOrdersOfCustomer() {
		return ordersOfCustomer;
	}
}
