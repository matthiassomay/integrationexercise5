package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;
import be.mobyus.ie5.entities.Order;
import be.mobyus.ie5.entities.OrderDetail;

import java.util.Scanner;

public class ListOrdersScreen extends AbstractScreen {

    @Override
    public String[] drawScreenInternal(EshopApplicationController controller) {

        System.out.println("Welcome " + controller.getCurrentUser().getFirstName() + ", you are now shopping in " + controller.getChosenShop().getName() + ".");
        System.out.println("You have made following orders:");
        System.out.println("Please enter to go back (q to quit):");

        for (Order order : controller.getOrdersOfCustomer()) {
            int i = 1;
            System.out.println(i + ". EUR " + order.getOrderTotal());
            for (OrderDetail detail : order.getOrderDetails()) {
                System.out.println("  - " + detail.getProduct().getName());
            }
            i++;


        }

        Scanner scanner = new Scanner(System.in);


        return new String[] { scanner.nextLine() };
    }

    private String[] showMainScreen(EshopApplicationController controller) {
        return new MainScreen().drawScreen(controller);
    }
}
