package be.mobyus.ie5.screens;

import be.mobyus.ie5.app.EshopApplicationController;

import static java.lang.System.exit;
import static java.lang.System.in;

import java.util.Scanner;

public abstract class AbstractScreen implements Screen {

	private final Scanner scanner = new Scanner(in);

	@Override
	public String[] drawScreen(EshopApplicationController controller) {
		for (int i = 0; i < 5; i++) {
			System.out.println("");
		}

		return drawScreenInternal(controller);
	}

	public abstract String[] drawScreenInternal(EshopApplicationController controller);

	public String readFromConsole(String prefix) {
		System.out.print("\n" + prefix + ":");
		String input = scanner.next();

		if ("q".equals(input)) {
			exit(0);
		}
		return input;
	}
}
