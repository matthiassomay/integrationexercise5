package be.mobyus.util;

import be.mobyus.ie5.entities.*;
import org.aspectj.weaver.ast.Or;
import org.hibernate.SessionFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InitialDataSetup {

	private TransactionTemplate transactionTemplate;
	private SessionFactory sessionFactory;

	public InitialDataSetup(TransactionTemplate transactionTemplate, SessionFactory sessionFactory) {
		this.transactionTemplate = transactionTemplate;
		this.sessionFactory = sessionFactory;
	}

	public void initData() {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {

				// Setup test data
				{

					Customer customer1 = new Customer();
					customer1.setFirstName("Walter");
					customer1.setName("White");
					customer1.setPassword(new Password("walterwhite"));
					customer1.setUserName("walter.white");

					Customer customer2 = new Customer();
					customer2.setFirstName("Jesse");
					customer2.setName("Pinkman");
					customer2.setPassword(new Password("jessepinkman"));
					customer2.setUserName("jesse.pinkman");

					Address address1 = new Address();
					address1.setStreet("Papendorpseweg");
					address1.setHouseNumber("100");
					address1.setCountry("The Netherlands");
					address1.setMunicipality("Utrecht");
					address1.setPostalCode("3528 BJ");
					address1.setBox("3");

					EshopInfo eshopInfo1 = new EshopInfo();
					eshopInfo1.setVatnumber("BE0824148721");
					eshopInfo1.setBankAccountNumber("NL27INGB0000026500");

					Address address2 = new Address();
					address2.setStreet("Borsbeeksebrug");
					address2.setHouseNumber("28");
					address2.setCountry("Belgium");
					address2.setMunicipality("Antwerpen");
					address1.setPostalCode("2600");
					address2.setBox("3");

					EshopInfo eshopInfo2 = new EshopInfo();
					eshopInfo2.setVatnumber("BE0867686774");
					eshopInfo2.setBankAccountNumber("BE85091220232102");

					Eshop bol = new Eshop();
					bol.setName("bol.com");
					bol.setEshopInfo(eshopInfo1);
					bol.setAddress(address1);
					sessionFactory.getCurrentSession().save(bol);

					Eshop coolblue = new Eshop();
					coolblue.setName("Coolblue");
					coolblue.setEshopInfo(eshopInfo2);
					coolblue.setAddress(address2);
					sessionFactory.getCurrentSession().save(coolblue);


					Product product1 = new Product();
					product1.setName("Karcher hogedrukreiniger");
					product1.setNumberInStock(5);
					product1.setPrice(new BigDecimal(120.00));
					product1.setShortName("hogedruk");
					sessionFactory.getCurrentSession().save(product1);

					Product product2 = new Product();
					product2.setName("Xbox One");
					product2.setNumberInStock(10);
					product2.setPrice(new BigDecimal(250.00));
					product2.setShortName("xbox");
					sessionFactory.getCurrentSession().save(product2);

					Product product3 = new Product();
					product3.setName("Calor strijkijzer");
					product3.setNumberInStock(8);
					product3.setPrice(new BigDecimal(150.00));
					product3.setShortName("calor");
					sessionFactory.getCurrentSession().save(product3);

					Product product4 = new Product();
					product4.setName("Samsung Galaxy S20+");
					product4.setNumberInStock(20);
					product4.setPrice(new BigDecimal(650.00));
					product4.setShortName("s20+");
					sessionFactory.getCurrentSession().save(product4);

					Product product5 = new Product();
					product5.setName("Asus ZenBook 13 inch ");
					product5.setNumberInStock(15);
					product5.setPrice(new BigDecimal(799.00));
					product5.setShortName("zenbook");
					sessionFactory.getCurrentSession().save(product5);

					OrderDetail detail1 = new OrderDetail();
					detail1.setProduct(product1);
					detail1.setAmount(new BigDecimal(1));
					detail1.setTotal(new BigDecimal(detail1.getProduct().getPrice().toString()));
					sessionFactory.getCurrentSession().save(detail1);

					OrderDetail detail2 = new OrderDetail();
					detail2.setProduct(product2);
					detail2.setAmount(new BigDecimal(1));
					detail2.setTotal(new BigDecimal(detail2.getProduct().getPrice().toString()));
					sessionFactory.getCurrentSession().save(detail2);

					List<OrderDetail> orderDetailList = new ArrayList<>();
					orderDetailList.add(detail1);
					orderDetailList.add(detail2);

					double total = 0;
					for (OrderDetail o : orderDetailList) {
						total += o.getTotal().doubleValue();

					}

					Order order1 = new Order();
					order1.setEshop(bol);
					order1.setOrderDetails(orderDetailList);
					order1.setPaymentMethod("MAESTRO");
					order1.setOrderTotal(new BigDecimal(total));
					order1.setCustomer(customer1);
					sessionFactory.getCurrentSession().save(order1);

					List<Order> orderForPeter = new ArrayList<>();
					orderForPeter.add(order1);

					//

					OrderDetail detail3 = new OrderDetail();
					detail3.setProduct(product3);
					detail3.setAmount(new BigDecimal(1));
					detail3.setTotal(new BigDecimal(detail3.getProduct().getPrice().toString()));
					sessionFactory.getCurrentSession().save(detail3);

					OrderDetail detail4 = new OrderDetail();
					detail4.setProduct(product4);
					detail4.setAmount(new BigDecimal(1));
					detail4.setTotal(new BigDecimal(detail4.getProduct().getPrice().toString()));
					sessionFactory.getCurrentSession().save(detail4);

					List<OrderDetail> orderDetailList2 = new ArrayList<>();
					orderDetailList2.add(detail3);
					orderDetailList2.add(detail4);

					double total2 = 0;
					for (OrderDetail o : orderDetailList2) {
						total2 += o.getTotal().doubleValue();

					}

					Order order2 = new Order();
					order2.setEshop(coolblue);
					order2.setOrderDetails(orderDetailList2);
					order2.setPaymentMethod("PayPal");
					order2.setOrderTotal(new BigDecimal(total2));
					order2.setCustomer(customer2);
					sessionFactory.getCurrentSession().save(order2);

					List<Order> orderForDirk = new ArrayList<>();
					orderForDirk.add(order2);



					customer1.setOrders(orderForPeter);
					sessionFactory.getCurrentSession().save(customer1);


					customer2.setOrders(orderForDirk);
					sessionFactory.getCurrentSession().save(customer2);




				}
			}
		});
	}

	public void tearDown() {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
			}
		});
	}
}
